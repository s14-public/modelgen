# Modelgen

This project serves to create CRUD functions from a Go struct which 
models a database table. To be used with a Postgres database.

## Usage

Clone this repository

cd into directory:

`cd modelgen`

build:

`go build -o gen cmd/main.go`

run:

`./gen -input cmd/test/widget.go -output model/widget.go -model Widget -params widgetParams`

When you first open your newly created file, run `gofmt` and `goimports` to format and 
import any missing packages. `errors` and `strings` will be on the list if you've included 
a params struct.

## CLI Params

**input**: input file with database table struct

**output**: output file with struct data and CRUD functions

**model**: name of model to operate on (can be one of many)

**params**: optional params struct for building SQL where queries

**intstr**: optional flag which, when sets app to accept integers/floats as strings in a JSON request

example with instr: 

`./gen -input cmd/test/widget.go -output model/widget.go -model Widget -params widgetParams -intstr`

I included this option, because I sometimes run into issues where JSON objects pass strings
instead of numbers. So, this lets you accept strings instead of numeric types, and assert the type in 
the backend. Obviously, a mixed case scenario will need special handling.

## CRUD Functions

Here are the functions created by this app (assuming struct is named Widget)

- CreateWidget

- UpdateWidget

- DeleteWidget

- GetWidgets

- GetWidget

GetWidgets take an interface type with optional search parameters, and returns a slice 
of Widgets. GetWidget, on the other hand, takes an int64 id, and returns a single Widget


## Params Function

The CLI takes an optional `params` argument. The argument is a struct that supplies 
a list of possible fields to search on. The datatypes on these fields need to be 
of the `sql.Null*` values in the `database/sql` package in the Go standard library.

https://golang.org/pkg/database/sql/#NullBool

For example, if we wanted the possibility to search for Name, Width, and CanRoll 
properties of a widget, we would create a struct like this:

``` 
type widgetParams struct {
	Name    sql.NullString
	Width   sql.NullInt32
	CanRoll sql.NullBool
}
```

Modelgen will generate a `buildWhereParams` function based on the supplied fields. It 
also generates a SQL Where query using these optional types. 

This will allow users to submit a JSON request with any number of possible search 
fields. Or none at all. These are all valid: 

```
{
	"name" : "Ball",
	"width" : 40
}
```

```
{
	"can_roll": true
}
```

```
{

}
```

## Project Notes

- This project is opinionated in the sense that it uses a single `model` package to 
define modeled database tables, and associated functions.

- If you need to use comparison operators, or more advance SQL queries, they need
implemented by hand. This only serves to generate basic queries.

- Check out `/cmd/test/widget.go` for an example struct

- This project **only works with Postgres**. The parameter syntax `$1,$2,$3` would need 
updated to fit an alternative database. Obviously, it wouldn't be too difficult, but
it's worth noting in case you wanted to use this with MariaDB, for instance.

- **You need to handle errors yourself**. Whether you use a logging package, print to the
terminal, or whatever, you still need to go through the code and implement error handling.

- The `/model/model.go` has a number of utility functions that are needed for this
project to run correctly. Namely, those that return `sql.Null*` types. 

- The `dbConn()` function in `/model/model.go` is where you need to supply a pointer
to your database connection.

- All functions, except Gets\* and Get\*, are transaction based. You will need to create
a transaction, and pass a pointer into each of these functions.

- All structs, which aren't a params struct, are assumed to have and `Id int64` field. 

## Todo

- Provide an option to supply a `shortName` variable since a model name trucated to 4 
characters might not work for every situation.

- Automatically wrap long lines in: SQL statements, statment.Exec functions, and row.Scan 
functions
