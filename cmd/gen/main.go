package main

import (
	"flag"
	"log"
	"os"

	"gitlab.com/s14-public/modelgen"
	"gitlab.com/s14-public/modelgen/define"
)

type Args struct {
	InputFile  string
	OutputFile string
	Model      string
	Params     string
	IntString  bool
}

func main() {

	input := flag.String("input", "", "file that has model struct")
	output := flag.String("output", "", "file to be created with functions")
	model := flag.String("model", "", "name of struct to create CRUD functions for")
	params := flag.String("params", "", "optional params struct for creating sql select where clause")
	ints := flag.Bool("intstr", false, "pass this flag if you want to pass strings in place of int32/64")
	flag.Parse()

	args := Args{
		InputFile:  *input,
		OutputFile: *output,
		Model:      *model,
		Params:     *params,
		IntString:  *ints}

	createFuncs(args)
}

func createFuncs(args Args) {
	var paramsModel define.Model
	var hasParams bool
	var err error

	file, err := os.Create(args.OutputFile)
	if err != nil {
		log.Println("couldn't create output file: ", err)
		os.Exit(1)
	}
	defer file.Close()

	//write existing file info first
	topLines, err := modelgen.GetFileLines(args.InputFile)
	if err != nil {
		log.Println("can't read input file: ", err)
	}

	for _, r := range topLines {
		file.Write([]byte(r))
		file.Write([]byte("\n"))
	}

	model, err := modelgen.ParseModel(args.InputFile, args.Model)
	if err != nil {
		log.Println("can't read input file: ", err)
	}

	if args.Params != "" {
		paramsModel, err = modelgen.ParseModel(args.InputFile, args.Params)
		hasParams = true
		if err != nil {
			log.Println("can't read input file: ", err)
		}
	}

	//create get functions and return a params function (optional)
	var get modelgen.Get
	get.Model = model
	get.HasParams = hasParams
	get.Params = paramsModel
	get.IntString = args.IntString
	get.File = file

	params := modelgen.CreateGet(get)

	//insert
	var insertIgnore []string
	insertIgnore = append(insertIgnore, "Id")
	modelgen.CreateInsert(model, file, insertIgnore)

	//update
	var updateIgnore []string
	//Id is automatically inserted as the SQL WHERE variable
	updateIgnore = append(updateIgnore, "Id")
	modelgen.CreateUpdate(model, file, updateIgnore)

	//delete
	modelgen.CreateDelete(model, file)

	//params
	modelgen.CreateParams(params, file)

	file.Close()
}
