package model

import (
	"database/sql"
	"time"
)

type Widget struct {
	Id           int64     `json:"id"`
	Name         string    `json:"name"`
	Height       int32     `json:"height"`
	Width        int32     `json:"width"`
	Weight       int64     `json:"weight"`
	Color        string    `json:"color"`
	CanRoll      bool      `json:"can_roll"`
	CanFly       bool      `json:"can_fly"`
	DateModified time.Time `json:"date_modified"`
}

type widgetParams struct {
	Name    sql.NullString
	Color   sql.NullString
	CanRoll sql.NullBool
	CanFly  sql.NullBool
	Weight  sql.NullInt64
	Height  sql.NullInt32
}
