package define

import (
	"fmt"
	"strings"
)

type Insert struct {
	Tables       string
	Placeholders string
	Fields       string
}

//creates a SQL insert function, along with associated Go code
func CreateFunc(model Model, ignore []string) ([]string, error) {
	var code []string

	//remove fields we want to ignore
	removeFields(&model, ignore)

	//generate variable names
	var shortName string
	lowerName := CamelToUnderscore(model.Name)

	if len(model.Name) >= 4 {
		shortName = strings.ToLower(model.Name[0:4])
	} else {
		shortName = fmt.Sprintf("%s_rec", lowerName) //lower and short can't be the same
	}

	funcDeclare := fmt.Sprintf("func Create%s(tx *sql.Tx, %s *%s) (int64, error) {\n",
		model.Name, shortName, model.Name)

	//add beginning of function
	code = append(code, funcDeclare)
	code = append(code, "var lastId int64\n\n")

	//generate and populate sql insert values
	insert := generateInsertValues(&model, shortName)

	sqlQuery := fmt.Sprintf("q := `INSERT INTO %s(%s) VALUES(%s) RETURNING id`\n\n", lowerName, insert.Tables, insert.Placeholders)
	code = append(code, sqlQuery)

	//generate insert statment
	code = append(code, makeTxPrepare(true))
	code = append(code, makeTxExec(insert.Fields, true))

	//end function
	code = append(code, "return lastId, nil\n}\n")

	return code, nil
}

//generate slices of data that update values of an insert query
//and the field names for an Exec function
func generateInsertValues(model *Model, shortName string) Insert {
	var insert Insert
	var tables []string
	var placeholders []string
	var fields []string

	//loop through and build lists up
	for i, f := range model.Fields {
		var table string
		var placeholder string
		var field string

		table = CamelToUnderscore(f.Name)
		placeholder = fmt.Sprintf("$%d", i+1)
		field = fmt.Sprintf("%s.%s", shortName, f.Name)

		tables = append(tables, table)
		placeholders = append(placeholders, placeholder)
		fields = append(fields, field)
	}

	insert.Tables = MakeCommaList(tables)
	insert.Placeholders = MakeCommaList(placeholders)
	insert.Fields = MakeCommaList(fields)

	return insert
}
