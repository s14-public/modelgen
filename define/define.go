package define

import (
	"fmt"
	"regexp"
	"strings"
)

type Model struct {
	Name   string
	Fields []Field
}

type Field struct {
	Name string
	Type string
}

//take a camel cased struct field and convert it to
//underscore ie. SomeData => some_data
func CamelToUnderscore(name string) string {
	re := regexp.MustCompile(`([A-Z][a-z]+)`)

	if !re.MatchString(name) {
		return fmt.Sprintf("somethingIsWeird__%s", name)
	}

	var underName strings.Builder

	matches := re.FindAllStringSubmatch(name, -1)
	size := len(matches)

	for i, m := range matches {
		lower := strings.ToLower(m[0])

		//if we're at the last element
		if i+1 == size {
			fmt.Fprintf(&underName, "%s", lower)
		} else {
			fmt.Fprintf(&underName, "%s_", lower)
		}
	}

	return underName.String()
}

//take a slice and return a comma separarted string
//that excludes a comma on the last item
func MakeCommaList(items []string) string {
	var list strings.Builder
	size := len(items)

	for i, r := range items {
		//if we're at the last element
		if i+1 == size {
			fmt.Fprintf(&list, "%s", r)
		} else {
			fmt.Fprintf(&list, "%s, ", r)
		}
	}

	return list.String()
}

//returns transaction prepare boilerplate
//true returns a result, false doesn't
func makeTxPrepare(result bool) string {
	var str string

	if result {
		str = `stmt, err := tx.Prepare(q)
	if err != nil {
		return lastId, err
	}
	defer stmt.Close()

	`

	} else {
		str = `stmt, err := tx.Prepare(q)
	if err != nil {
		return err
	}
	defer stmt.Close()

	`
	}

	return str

}

//returns a transaction execute statment
//with included parameters
func makeTxExec(params string, result bool) string {
	var stmt string
	var end string

	if result {
		stmt = fmt.Sprintf("err = stmt.QueryRow(%s).Scan(&lastId)\n", params)

		end = `
	if err != nil {
		return lastId, err
	}
	`
	} else {
		stmt = fmt.Sprintf("_, err = stmt.Exec(%s)\n", params)

		end = `
	if err != nil {
		return err
	}
	`
	}

	return fmt.Sprintf("%s%s\n", stmt, end)
}

//remove fields that you don't want to include in a function
//supply a list of fields to the names param to be excluded
func removeFields(model *Model, names []string) {
	if len(names) == 0 {
		return
	}

	var fields []Field
	ignore := make(map[string]int)

	for i, f := range model.Fields {
		for _, n := range names {
			if f.Name == n {
				if _, ok := ignore[f.Name]; ok {
					continue
				}

				ignore[f.Name] = i
				continue
			}
		}

		if _, ok := ignore[f.Name]; ok {
			continue
		}

		fields = append(fields, Field{Name: f.Name, Type: f.Type})
	}

	model.Fields = fields
}
