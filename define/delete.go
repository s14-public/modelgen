package define

import (
	"fmt"
)

func DeleteFunc(model Model) ([]string, error) {
	var code []string

	lowerName := CamelToUnderscore(model.Name)

	funcDeclare := fmt.Sprintf("func Delete%s(tx *sql.Tx, id int64) error {\n\n",
		model.Name)

	code = append(code, funcDeclare)
	code = append(code, fmt.Sprintf("q :=`DELETE FROM %s WHERE id=$1`\n\n", lowerName))
	code = append(code, makeTxPrepare(false))
	code = append(code, makeTxExec("id", false))
	code = append(code, "return nil\n}\n")

	return code, nil
}
