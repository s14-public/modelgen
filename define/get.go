package define

import (
	"errors"
	"fmt"
	"log"
	"strings"
)

type Get struct {
	ParamFields string
	ScanFields  string
}

type NullType struct {
	Value     string
	NullValue string
}

//these are null types where ints stay ints
var nullTypeInt = map[string]NullType{
	"sql.NullInt64":   {Value: "GetNullInt64FromFloat", NullValue: "GetNullInt64(0, false)"},
	"sql.NullInt32":   {Value: "GetNullInt32FromFloat", NullValue: "GetNullInt32(0, false)"},
	"sql.NullFloat64": {Value: "GetNullFloat64", NullValue: "GetNullFloat64(0, false)"},
	"sql.NullString":  {Value: "GetNullString", NullValue: "GetNullString(\"\", false)"},
	"sql.NullBool":    {Value: "GetNullBool", NullValue: "GetNullBool(false, false)"},
	"sql.NullTime":    {Value: "GetNullTime", NullValue: "GetNullTime(\"0\", false)"},
}

//these are null types where ints are passed as strings
var nullTypeString = map[string]NullType{
	"sql.NullInt64":   {Value: "GetNullInt64FromString", NullValue: "GetNullInt64(0, false)"},
	"sql.NullInt32":   {Value: "GetNullInt32FromString", NullValue: "GetNullInt32(0, false)"},
	"sql.NullFloat64": {Value: "GetNullFloat64FromString", NullValue: "GetNullFloat64(0, false)"},
	"sql.NullString":  {Value: "GetNullString", NullValue: "GetNullString(\"\", false)"},
	"sql.NullBool":    {Value: "GetNullBool", NullValue: "GetNullBool(false, false)"},
	"sql.NullTime":    {Value: "GetNullTime", NullValue: "GetNullTime(\"0\", false)"},
}

type NullMap map[string]NullType

//generate values for creating whereParams function
//true lets strings be passed as ints, false leaves numeric types
func generateIntStringValue(b bool) NullMap {
	var nullMap NullMap

	if b {
		nullMap = nullTypeString
	} else {
		nullMap = nullTypeInt
	}

	return nullMap
}

//creates a SQL select function, with option where clause
func GetsFunc(model Model, paramsModel Model, whereSql string) ([]string, error) {
	var code []string

	//remove and fields we want to ignore
	//removeFields(&model, ignore)

	//generate names
	shortName := strings.ToLower(model.Name[0:4])

	//generate values for statement and scan
	get := generateGetValues(&model, &paramsModel, shortName)

	funcDeclare := fmt.Sprintf("\nfunc Get%[1]ss(r interface{}) ([]%[1]s, error) {\n",
		model.Name)

	//add beginning of function
	code = append(code, funcDeclare)
	code = append(code, "db := dbConn()\n\n")
	code = append(code, fmt.Sprintf("var %ss []%s\n\n", shortName, model.Name))

	//add params section if there are fields to add
	if len(paramsModel.Fields) > 0 {
		code = append(code, fmt.Sprintf("var params %s\n", paramsModel.Name))
		code = append(code, "paramSet := params.buildWhereParams(r)\nif !paramSet {\n")
		code = append(code, fmt.Sprintf("return %ss, errors.New(\"no search params were set\")\n", shortName))
		code = append(code, "}\n\n")
	}

	//add sql query
	code = append(code, whereSql)

	//generate get statment
	code = append(code, "//run sql query\n")
	code = append(code, "stmt, err := db.Prepare(q)\nif err != nil {\n")
	code = append(code, fmt.Sprintf("return %ss, err\n}\n", shortName))
	code = append(code, "defer stmt.Close()\n\n")

	//run query and add search params
	code = append(code, fmt.Sprintf("rows, err := stmt.Query(%s)\n\n", get.ParamFields))
	code = append(code, fmt.Sprintf("if err != nil {\n return %ss, err\n}\n", shortName))
	code = append(code, "defer rows.Close()\n\n")

	//run scan loop
	code = append(code, "//scan returned rows into a slice\n")
	code = append(code, "for rows.Next() {\n")
	code = append(code, fmt.Sprintf("var %s %s\n\n", shortName, model.Name))
	code = append(code, fmt.Sprintf("err = rows.Scan(%s)\n\n", get.ScanFields))
	code = append(code, "if err != nil {\n if err == rows.Err() {\n //handle error \n")
	code = append(code, "}\n}\n\n")
	code = append(code, fmt.Sprintf("%[1]ss = append(%[1]ss, %[1]s)\n", shortName))
	code = append(code, "}\n\n")

	//end function
	code = append(code, fmt.Sprintf("return %ss, nil}\n", shortName))

	return code, nil
}

//generates a function that performs a SELECT query, based on id, that returns
//one row
func GetFunc(model Model) ([]string, error) {
	var code []string

	shortName := strings.ToLower(model.Name[0:4])
	lowerName := CamelToUnderscore(model.Name)

	funcDeclare := fmt.Sprintf("func Get%[1]s(id int64) (%[1]s, error) {\n", model.Name)
	code = append(code, funcDeclare)
	code = append(code, "db := dbConn()\n\n")
	code = append(code, fmt.Sprintf("var %s %s\n\n", shortName, model.Name))

	//sql statement
	sqlQuery := fmt.Sprintf("q := `SELECT * FROM %s WHERE id=$1`\n\n", lowerName)
	code = append(code, sqlQuery)

	//generate values for statement and scan
	get := generateGetValues(&model, &Model{}, shortName)

	//generate get statment
	code = append(code, "//run sql query\n")
	code = append(code, "stmt, err := db.Prepare(q)\nif err != nil {\n")
	code = append(code, fmt.Sprintf("return %s, err\n}\n", shortName))
	code = append(code, "defer stmt.Close()\n\n")

	code = append(code, fmt.Sprintf("err = stmt.QueryRow(id).Scan(%s)\n", get.ScanFields))
	code = append(code, fmt.Sprintf("if err != nil {\n return %s, err\n}\n\n", shortName))

	//return and finish function
	code = append(code, fmt.Sprintf("return %s,nil\n}\n\n", shortName))

	return code, nil
}

//generate a search params function and create an SQL where clause
func GenerateSearchParams(model Model, searchTable string, intStrings bool) ([]string, string, error) {
	var code []string
	var whereStatement strings.Builder

	nullMap := generateIntStringValue(intStrings)

	funcDeclare := fmt.Sprintf("func (params *%s) buildWhereParams(r interface{}) bool {\n", model.Name)
	code = append(code, funcDeclare)
	code = append(code, "var oneParamSet bool = false\n\n")
	code = append(code, "if m, ok := r.(map[string]interface{}); ok {\n")

	//build first part of SQL statment
	whereStatement.WriteString(fmt.Sprintf("q := `SELECT * FROM %s WHERE ", searchTable))

	fieldSize := len(model.Fields)

	//loop through each struct field and build a paramater
	for i, f := range model.Fields {
		var err error

		lowerName := CamelToUnderscore(f.Name)
		code, err = generateIfBlock(code, f.Name, lowerName, f.Type, nullMap)
		if err != nil {
			log.Println(err)
		}

		//generates a SQL where clause as we loop
		err = generateWhereClause(lowerName, f.Type, &whereStatement, i+1)
		if err != nil {
			log.Println(err)
		}

		//add an "AND" to our SQL query if we're not at the last parameter
		if i+1 < fieldSize {
			whereStatement.WriteString(" AND\n")
		}
	}

	//add ending backtick in SQL query
	whereStatement.WriteString("`\n\n")

	//end of if statement
	code = append(code, "}\n\n")

	//end of function
	code = append(code, "return oneParamSet\n}\n\n")

	return code, whereStatement.String(), nil
}

//generate an if statement that is based on the sql.Null* type we're creating
func generateIfBlock(code []string, name string, lowerName string, dataType string, nullMap NullMap) ([]string, error) {
	var nullFunc NullType
	var ifText strings.Builder

	if _, ok := nullMap[dataType]; !ok {
		err := errors.New(fmt.Sprintf("couldn't map type: %s", dataType))
		return code, err
	}

	nullFunc = nullMap[dataType]

	//format our if statement based on datatype
	switch dataType {
	case "sql.NullInt64":
		ifText.WriteString("if m[\"%[1]s\"] != nil && m[\"%[1]s\"] != \"\" {\n")
		ifText.WriteString("paramValue := m[\"%[1]s\"]\n")

	case "sql.NullInt32":
		ifText.WriteString("if m[\"%[1]s\"] != nil && m[\"%[1]s\"] != \"\" {\n")
		ifText.WriteString("paramValue := m[\"%[1]s\"]\n")

	case "sql.NullFloat64":
		ifText.WriteString("if m[\"%[1]s\"] != nil && m[\"%[1]s\"] != \"\" {\n")
		ifText.WriteString("paramValue := m[\"%[1]s\"]\n")

	case "sql.NullString":
		ifText.WriteString("if m[\"%[1]s\"] != nil && m[\"%[1]s\"] != \"\" {\n")
		ifText.WriteString("paramValue := strings.ToLower(m[\"%[1]s\"].(string))\n")

	case "sql.NullBool":
		//bool is special because we have to check twice
		ifText.WriteString("if m[\"%[1]s\"] != nil && m[\"%[1]s\"] != \"\" {\n")
		ifText.WriteString("paramValue, ok := m[\"%[1]s\"].(bool)\n")
		ifText.WriteString("if ok {\n")
		code = append(code, fmt.Sprintf(ifText.String(), lowerName))
		code = append(code, fmt.Sprintf("params.%s = %s(paramValue, true)\n", name, nullFunc.Value))
		code = append(code, "oneParamSet = true\n")
		code = append(code, "} else {\n")
		code = append(code, fmt.Sprintf("params.%s = %s\n}\n\n", name, nullFunc.NullValue))

		code = append(code, "} else {\n")
		code = append(code, fmt.Sprintf("params.%s = %s\n}\n\n", name, nullFunc.NullValue))

		return code, nil

	case "sql.NullTime":
		ifText.WriteString("if m[\"%[1]s\"] != nil && m[\"%[1]s\"] != \"\" {\n")
		ifText.WriteString("paramValue := m[\"%[1]s\"]\n")
	}

	//add body of if statement
	code = append(code, fmt.Sprintf(ifText.String(), lowerName))
	code = append(code, fmt.Sprintf("params.%s = %s(paramValue, true)\n", name, nullFunc.Value))
	code = append(code, "oneParamSet = true\n")
	code = append(code, "} else {\n")
	code = append(code, fmt.Sprintf("params.%s = %s\n}\n\n", name, nullFunc.NullValue))

	return code, nil
}

//build an SQL where statemnt using optional types
//based on the model struct passed in
func generateWhereClause(lowerName string, dataType string, whereText *strings.Builder, index int) error {
	switch dataType {
	case "sql.NullInt64":
		whereText.WriteString(fmt.Sprintf("($%[1]d::bigint is NULL or %s = $%[1]d)", index, lowerName))
	case "sql.NullInt32":
		whereText.WriteString(fmt.Sprintf("($%[1]d::integer is NULL or %s = $%[1]d)", index, lowerName))
	case "sql.NullFloat64":
		whereText.WriteString(fmt.Sprintf("($%[1]d::decimal is NULL or %s = $%[1]d)", index, lowerName))
	case "sql.NullString":
		whereText.WriteString(fmt.Sprintf("($%[1]d::text is NULL or %s ILIKE '%%' || $%[1]d || '%%')", index, lowerName))
	case "sql.NullBool":
		whereText.WriteString(fmt.Sprintf("($%[1]d::boolean is NULL or %s = $%[1]d)", index, lowerName))
	case "sql.NullTime":
		whereText.WriteString(fmt.Sprintf("($%[1]d::date is NULL or %s = $%[1]d)", index, lowerName))
	}

	return nil
}

func generateGetValues(model *Model, paramsModel *Model, shortName string) Get {
	var get Get
	var paramFields []string
	var scanFields []string

	for _, p := range paramsModel.Fields {
		paramFields = append(paramFields, fmt.Sprintf("params.%s", p.Name))
	}

	for _, f := range model.Fields {
		scanFields = append(scanFields, fmt.Sprintf("&%s.%s", shortName, f.Name))
	}

	get.ParamFields = MakeCommaList(paramFields)
	get.ScanFields = MakeCommaList(scanFields)

	return get
}
