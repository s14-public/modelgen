package define

import (
	"fmt"
	"strings"
)

type Update struct {
	Placeholders string
	Fields       string
}

type Placeholder struct {
	Field string
	Place string
}

func UpdateFunc(model Model, ignore []string) ([]string, error) {
	var code []string

	//remove fields we want to ignore
	removeFields(&model, ignore)

	shortName := strings.ToLower(model.Name[0:4])
	lowerName := CamelToUnderscore(model.Name)

	funcDeclare := fmt.Sprintf("func Update%s(tx *sql.Tx, %s *%s) error {\n\n",
		model.Name, shortName, model.Name)

	//add beginning of function
	code = append(code, funcDeclare)

	update := generateUpdateValues(&model, shortName)

	//create update statement
	sqlQuery := fmt.Sprintf("q := `UPDATE %s SET %s WHERE id=$1`\n\n", lowerName,
		update.Placeholders)

	code = append(code, sqlQuery)

	//generate update statement
	code = append(code, makeTxPrepare(false))
	code = append(code, makeTxExec(update.Fields, false))

	code = append(code, "return nil\n}\n")

	return code, nil
}

func generateUpdateValues(model *Model, shortName string) Update {
	var update Update
	var placeholders []Placeholder
	var updateFields []string //in the sql statement
	var fields []string       //in the exec statement

	//insert id as the first field for updating
	fields = append(fields, fmt.Sprintf("%s.Id", shortName))

	//loop and build update clause
	for i, f := range model.Fields {
		var field string

		var placeholder Placeholder
		placeholder.Field = CamelToUnderscore(f.Name)
		placeholder.Place = fmt.Sprintf("$%d", i+2)

		field = fmt.Sprintf("%s.%s", shortName, f.Name)

		placeholders = append(placeholders, placeholder)
		fields = append(fields, field)
	}

	for _, p := range placeholders {
		u := fmt.Sprintf("%s=%s", p.Field, p.Place)
		updateFields = append(updateFields, u)
	}

	update.Placeholders = MakeCommaList(updateFields)
	update.Fields = MakeCommaList(fields)

	return update
}
