package model

import (
	"database/sql"
	"strconv"
	"time"
)

type Where interface {
	buildWhereQuery(r interface{}) (string, []interface{})
}

//return pointer to search db connection
func DbConn() *sql.DB {
	var db *sql.DB

	//you need to implement your own sql.DB

	return db
}

//handle transaction commit, and rollback if there's a failure
func CommitTx(tx *sql.Tx) error {
	err := tx.Commit()
	if err != nil {
		//handle error
		err = tx.Rollback()
		if err != nil {
			//handle error
			return err
		}

		return err
	}

	return nil
}

///////////////////////////////////////////////////////
// Utility functions for getting nullable data types //
///////////////////////////////////////////////////////

func MapNullType(i interface{}, dataType string, res interface{}) {
	//map interface to a nullable type to be used with
	//optional where statements in a postgres database
	//
	//dataTypes are: "string", "int"

	if dataType == "int" {
		var nullInt sql.NullInt64

		if i != nil && i != "" && i != "0" {
			nullInt = GetNullInt64FromString(i, true)
		} else {
			nullInt = GetNullInt64(0, true)
		}

		res = &nullInt
	}
}

func GetNullInt64(i int64, b bool) sql.NullInt64 {
	if b {
		return sql.NullInt64{
			Int64: i,
			Valid: true,
		}
	}

	return sql.NullInt64{}
}

func GetNullInt32(i int32, b bool) sql.NullInt32 {
	if b {
		return sql.NullInt32{
			Int32: i,
			Valid: true,
		}
	}

	return sql.NullInt32{}
}

func GetNullInt64FromFloat(i interface{}, b bool) sql.NullInt64 {
	if b {
		//first map to float
		f := i.(float64)
		v := int64(f)

		return sql.NullInt64{
			Int64: v,
			Valid: true,
		}
	}

	return sql.NullInt64{}
}

func GetNullInt32FromFloat(i interface{}, b bool) sql.NullInt32 {
	if b {
		//first map to float
		f := i.(float64)
		v := int32(f)

		return sql.NullInt32{
			Int32: v,
			Valid: true,
		}
	}

	return sql.NullInt32{}
}

func GetNullFloat64(i interface{}, b bool) sql.NullFloat64 {
	if b {
		//first map to float
		f := i.(float64)

		return sql.NullFloat64{
			Float64: f,
			Valid:   true,
		}
	}

	return sql.NullFloat64{}
}

func GetNullInt64FromString(i interface{}, b bool) sql.NullInt64 {
	if b {
		intConv, _ := strconv.ParseInt(i.(string), 10, 64)

		return sql.NullInt64{
			Int64: intConv,
			Valid: true,
		}
	}

	return sql.NullInt64{}
}

func GetNullInt32FromString(i interface{}, b bool) sql.NullInt32 {
	if b {
		intConv, _ := strconv.ParseInt(i.(string), 10, 32)

		return sql.NullInt32{
			Int32: int32(intConv),
			Valid: true,
		}
	}

	return sql.NullInt32{}
}

func GetNullFloat64FromString(i interface{}, b bool) sql.NullFloat64 {
	if b {
		floatConv, _ := strconv.ParseFloat(i.(string), 64)

		return sql.NullFloat64{
			Float64: floatConv,
			Valid:   true,
		}
	}

	return sql.NullFloat64{}
}

func GetNullString(s string, b bool) sql.NullString {
	if b {
		return sql.NullString{
			String: s,
			Valid:  true,
		}
	}

	return sql.NullString{}
}

func GetNullBool(s bool, b bool) sql.NullBool {
	if b {
		if s == true {
			return sql.NullBool{
				Bool:  true,
				Valid: true,
			}
		} else {
			return sql.NullBool{
				Bool:  false,
				Valid: true,
			}
		}
	}

	return sql.NullBool{}
}

func GetNullTime(i interface{}, b bool) sql.NullTime {
	if b {
		//assert interface to string
		s := i.(string)

		timeFormat := "2006-01-02"
		t, _ := time.Parse(timeFormat, s)

		return sql.NullTime{
			Time:  t,
			Valid: true,
		}
	}

	return sql.NullTime{}
}
