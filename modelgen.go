package modelgen

import (
	"fmt"
	"gitlab.com/s14-public/fileutil"
	"log"
	"os"
	"regexp"

	"gitlab.com/s14-public/modelgen/define"
)

type Get struct {
	Model     define.Model
	HasParams bool
	Params    define.Model
	IntString bool
	File      *os.File
}

//get existing file lines to write in first
//this just wraps fileutil's OpenFileSlice
func GetFileLines(filepath string) ([]string, error) {
	var lines []string

	lines, err := fileutil.OpenFileSlice(filepath)
	if err != nil {
		return lines, err
	}

	return lines, nil
}

//read a struct into a Model object
func ParseModel(filepath string, modelName string) (define.Model, error) {
	var model define.Model

	lines, err := fileutil.OpenFileSlice(filepath)
	if err != nil {
		return model, err
	}

	//define regex's we want to match
	regStruct := regexp.MustCompile(`type\s(\w*)\sstruct`)
	regField := regexp.MustCompile(`(\w+)\s+([\w\.]+)`)
	regEnd := regexp.MustCompile(`\}`)
	var structStart bool

	for _, l := range lines {
		//check for struct name
		if regStruct.MatchString(l) && !structStart {
			matches := regStruct.FindStringSubmatch(l)

			//make sure this is the struct we're looking
			//to generate models from
			if modelName != matches[1] {
				continue
			}

			model.Name = matches[1]
			structStart = true
			continue
		}

		//grab field names
		if regField.MatchString(l) && structStart {
			matches := regField.FindStringSubmatch(l)
			model.Fields = append(model.Fields, define.Field{Name: matches[1], Type: matches[2]})
			continue
		}

		//get end of struct and reset model
		if regEnd.MatchString(l) && structStart {
			break
		}
	}

	return model, nil
}

func CreateInsert(model define.Model, file *os.File, ignore []string) {
	insertCode, err := define.CreateFunc(model, ignore)
	if err != nil {
		log.Println(err)
	}

	for _, r := range insertCode {
		file.Write([]byte(r))
	}

	file.Write([]byte("\n"))
}

func CreateUpdate(model define.Model, file *os.File, ignore []string) {
	updateCode, err := define.UpdateFunc(model, ignore)
	if err != nil {
		log.Println(err)
	}

	for _, r := range updateCode {
		file.Write([]byte(r))
	}

	file.Write([]byte("\n"))
}

func CreateDelete(model define.Model, file *os.File) {
	deleteCode, err := define.DeleteFunc(model)
	if err != nil {
		log.Println(err)
	}

	for _, r := range deleteCode {
		file.Write([]byte(r))
	}

	file.Write([]byte("\n"))
}

func CreateGet(get Get) []string {
	var whereSql string
	var paramCode []string
	var err error

	if get.HasParams {
		paramCode, whereSql, err = define.GenerateSearchParams(get.Params, define.CamelToUnderscore(get.Model.Name), get.IntString)
		if err != nil {
			log.Println(err)
		}
	} else {
		whereSql = fmt.Sprintf("q :=`SELECT * FROM %s`\n", define.CamelToUnderscore(get.Model.Name))
	}

	//slice return with params function
	getsCode, err := define.GetsFunc(get.Model, get.Params, whereSql)
	if err != nil {
		log.Println(err)
	}

	for _, r := range getsCode {
		get.File.Write([]byte(r))
	}

	get.File.Write([]byte("\n"))

	//single row return function
	getCode, err := define.GetFunc(get.Model)
	if err != nil {
		log.Println(err)
	}

	for _, r := range getCode {
		get.File.Write([]byte(r))
	}

	get.File.Write([]byte("\n"))

	return paramCode
}

func CreateParams(params []string, file *os.File) {
	if len(params) == 0 {
		return
	}

	for _, r := range params {
		file.Write([]byte(r))
	}
}
